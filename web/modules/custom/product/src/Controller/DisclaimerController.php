<?php

/**
 * @file
 * Contains \Drupal\product\Controller\DisclaimerController.
 */

namespace Drupal\product\Controller;

use Drupal\Core\Controller\ControllerBase;


class DisclaimerController extends ControllerBase {


  /**
   * {@inheritdoc}
   */
  public function output() {
    $disclaimerForm = \Drupal::formBuilder()->getForm('Drupal\product\Form\DisclaimerForm');

    return $disclaimerForm;
  }
}
