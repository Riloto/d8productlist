# Emergya BackEnd Drupal Test Project

This project creates a new Drupal 8 site with a new Content Type called "Product" with the next structure:

* **"id"**: 0,
* **"title"**: "SAMSUNG 970 EVO Plus SSD 1TB",
* **"body"**: "Lorem ipsum dolor sit amet, consectetur.",
* **"field_qty"**: 23 ,
* **"field_price"**: 129.99 ,
* **"field_image"**: "samsung970evoplus.png",
* **"field_protected"**: TRUE

The site has a view that lists all products in stock as Teaser view mode.
It also adds a disclaimer page for Anonymous users who want to access protected products;
this information is stored in a cookie.

This project provides a site that manages its dependencies with [Composer](https://getcomposer.org/).

## Usage

First you need to [install DDEV](https://ddev.com/get-started/).

## Starting Project

Follow the steps below to start the project using DDEV.

1. Run `git clone https://gitlab.com/Riloto/d8productlist.git` to clone the code of the repository.
2. Run `ddev start` to start the ddev project.
3. Run `ddev drush cim -y` to import configuration and run `ddev drush cr` to clear all caches.
4. Run `ddev drush genc 30 --bundles=product` to generate 30 test products.
5. Run `ddev drush uli` to obtain a one-usage url to access as admin.


Authored by: **Ricardo López Toro**
