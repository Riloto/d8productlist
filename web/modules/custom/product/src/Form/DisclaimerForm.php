<?php

namespace Drupal\product\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Cookie;

/**
 * Implements the Disclaimer form controller.
 *
 * @see \Drupal\Core\Form\FormBase
 */
class DisclaimerForm extends FormBase {

  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['disclaimer'] = array(
      '#type' => 'markup',
      '#markup' => '<h1>To gain access you must agree to the terms and conditions</h1>',
      '#weight' => '0',
    );

    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => $this->t('Accept'),
    );

    return $form;
  }
  public function getFormId() {
    return 'product_disclaimer_form';
  }
  public function submitForm(array &$form, FormStateInterface $form_state) {
    user_cookie_save(['disclaim' => "TRUE"]);
    $form_state->setRedirect('<front>');
    return;
  }
}
